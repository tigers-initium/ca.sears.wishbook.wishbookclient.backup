import {Component} from '@angular/core';
import {ProfileIconComponent} from '../ProfileIcon/ProfileIcon';

@Component({
    selector: 'ProfileHeader',
    templateUrl: 'build/components/ProfileHeader/ProfileHeader.html',
    inputs: ['profile', 'title'],
    directives: [ProfileIconComponent]
})
export class ProfileHeaderComponent {

    public profile: Object;
    public title: string;
    constructor() {
    }
}
