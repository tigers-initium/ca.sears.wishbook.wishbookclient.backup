import {Component} from '@angular/core';
import {Events} from 'ionic-angular';
import {Validators, FormBuilder, FormControl} from '@angular/forms';
import {TranslatePipe} from 'ng2-translate';
// import {validateBadWords} from '../../Validators/Validators';

@Component({
    selector: 'SignupName',
    templateUrl: 'build/components/Signup/SignupName/SignupName.html',
    pipes: [TranslatePipe],
})

export class SignupNameComponent {

    signup: Object;

    constructor(private formBuilder: FormBuilder,
                public events: Events) {
        console.log("Initializing");
    }

    ngOnInit() {
        this.signup = this.formBuilder.group({
            name: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(15)]]
        });
    }

    parentSliderNext() {
        let value = this.signup['value'];
        this.events.publish('signup:save::name', value);
        this.events.publish('signup:next', 1);
    }

}
