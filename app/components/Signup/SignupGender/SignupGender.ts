import {Component} from '@angular/core';
import {Validators, FormBuilder, FormControl} from '@angular/forms';
import {Events} from 'ionic-angular';
import {GenderIconComponent} from '../../GenderIcon/GenderIcon';

@Component({
    selector: 'SignupGender',
    directives: [GenderIconComponent],
    templateUrl: 'build/components/Signup/SignupGender/SignupGender.html'
})

export class SignupGenderComponent {
    gender: string;
    genders: Array<string>;

    constructor(private formBuilder: FormBuilder, public events: Events) {
        this.gender = null;
        this.genders = ['girl', 'boy'];
    }

    selectGender($event) {
        console.log("Selected gender", $event);

        if (this.gender == $event) {
            this.gender = null;
        } else {
            this.gender = $event;
        }
    }

    parentSliderNext() {
        this.events.publish('signup:save::gender', this.gender);
        this.events.publish('signup:next', 1);
    }

}
