import {Component} from '@angular/core';
import {Events} from 'ionic-angular';
import {Validators, FormBuilder, FormControl} from '@angular/forms';

@Component({
    selector: 'SignupAge',
    inputs: ['name'],
    templateUrl: 'build/components/Signup/SignupAge/SignupAge.html'
})

export class SignupAgeComponent {

    signup: Object;

    constructor(private formBuilder: FormBuilder,
    public events: Events) {
    }

    ngOnInit() {
        this.signup = this.formBuilder.group({
            age: ['']
        });
    }

    parentSliderNext() {

        let value = this.signup['value'];

        this.events.publish('signup:next', 1);
        this.events.publish('signup:save::age', value);
    }

}
