import {Component, ViewChild} from '@angular/core';
import {IONIC_DIRECTIVES, Slides} from 'ionic-angular';

@Component({
    selector: 'ProductCarousel',
    inputs: ['product'],
    templateUrl: 'build/components/ProductCarousel/ProductCarousel.html',
    directives: [IONIC_DIRECTIVES] // makes all Ionic directives available to your component
})
export class ProductCarousel {
    public product: Object;
    public images: any;
    public active: number;

    @ViewChild('ProductCarousel') slider: Slides;

    ngOnInit() {

        this.active = 0;

        if (!this.product) {
            console.log('No product found');
        } else {

            let i = [];
            let p = this.product;

            if (!(('images' in p) && (p['images']).length > 0)) {
                i = [p['url'], p['url'], p['url']];
            } else i = p['images'];

            this.images = i;

        }
    }

    goToSlide(index) {
        this.slider.slideTo(index, 500);
    }

    onSlideChanged() {
        const max = this.slider.length();
        this.active = this.slider.getActiveIndex();
    }

}
