import {Component} from '@angular/core';
import {NavParams, Events} from 'ionic-angular';
import {Validators, FormBuilder, FormControl} from '@angular/forms';
import {validateEmail} from '../Validators/Validators';
import {TranslatePipe} from 'ng2-translate';

@Component({
    selector: 'signup-form',
    templateUrl: 'build/components/SignupForm/SignupForm.html',
    pipes: [TranslatePipe]
})
export class SignupFormComponent {

    signup: Object;

    constructor(private formBuilder: FormBuilder,
                public navParams: NavParams,
                public events: Events) {}

    ngOnInit() {
        this.signup = this.formBuilder.group({
            name: ['', [Validators.required, Validators.minLength(3)]],
            email: ['', validateEmail],
            age: ['', Validators.required],
            gender: [''],
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        let results = JSON.stringify(this.signup['value']);
        this.events.publish('request:createProfile', results);
    }

}
