import {Component} from '@angular/core';

@Component({
    selector: 'GenderIcon',
    inputs: ['gender','selected'],
    templateUrl: 'build/components/GenderIcon/GenderIcon.html'
})

export class GenderIconComponent {
    gender: string;
    selected: boolean;
    name: string;
    constructor() {

    }

    ngOnInit() {
        let name = this.gender;
        this.name = this.capitalizeString(name);
    }

    capitalizeString(str) {
        let result = str.charAt(0).toUpperCase() + str.slice(1);
        return result;
    }

}
