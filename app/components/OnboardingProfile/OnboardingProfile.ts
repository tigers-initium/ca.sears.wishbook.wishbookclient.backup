import {Component} from '@angular/core';
import {TranslatePipe} from 'ng2-translate';

@Component({
    selector: 'OnboardingProfilePage',
    pipes: [TranslatePipe],
    templateUrl: 'build/components/OnboardingProfile/OnboardingProfile.html',
})
export class OnboardingProfileComponent {

}
