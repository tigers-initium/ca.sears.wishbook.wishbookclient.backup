import {Component} from '@angular/core';
import {TranslatePipe} from 'ng2-translate';

@Component({
    selector: 'OnboardingListPage',
    pipes: [TranslatePipe],
    templateUrl: 'build/components/OnboardingList/OnboardingList.html',
})
export class OnboardingListComponent {

}
