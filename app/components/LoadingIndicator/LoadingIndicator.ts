import {Component} from '@angular/core';
import {TranslatePipe} from 'ng2-translate';

@Component({
    selector: 'LoadingIndicator',
    pipes: [TranslatePipe],
    templateUrl: 'build/components/LoadingIndicator/LoadingIndicator.html'
})

export class LoadingIndicator {

}
