import {Component} from '@angular/core';
import {TranslatePipe} from 'ng2-translate';

@Component({
    selector: 'OnboardingEmailPage',
    pipes: [TranslatePipe],
    templateUrl: 'build/components/OnboardingEmail/OnboardingEmail.html',
})
export class OnboardingEmailComponent {

}
