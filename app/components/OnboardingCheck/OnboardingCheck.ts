import {Component} from '@angular/core';
import {TranslatePipe} from 'ng2-translate';

@Component({
    selector: 'OnboardingCheckPage',
    pipes: [TranslatePipe],
    templateUrl: 'build/components/OnboardingCheck/OnboardingCheck.html',
})
export class OnboardingCheckComponent {

}
