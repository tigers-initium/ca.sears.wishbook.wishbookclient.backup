import {Component} from '@angular/core';
import {TranslatePipe} from 'ng2-translate';
import {
    FormBuilder,
    FormArray,
    FormGroup,
    REACTIVE_FORM_DIRECTIVES
} from '@angular/forms';
import {Events} from 'ionic-angular';
import {validateEmail} from '../Validators/Validators';

@Component({
    selector: 'settingsEmailForm',
    templateUrl: 'build/components/SettingsEmailForm/SettingsEmailForm.html',
    pipes: [TranslatePipe],
    directives: [REACTIVE_FORM_DIRECTIVES]
})
export class SettingsEmailForm {

    // TODO: populate form with saved values

    public settingsEmail: FormGroup;
    helpers: Array<string>;

    constructor(private formBuilder: FormBuilder,
                public events: Events) {
        this.helpers = [''];
    }

    ngOnInit() {
        console.log('Setup form');
        this.settingsEmail = this.formBuilder.group({
            email: ['', validateEmail],
            helper_emails: this.formBuilder.array([
                this.initHelpers(),
            ])
        });
    }

    initHelpers() {
        return this.formBuilder.group({
            helper_email: ['']
        });
    }

    addHelper() {
        event.preventDefault();
        console.log('Add helper');
        // this.helpers.push('');
        const control = <FormArray>this.settingsEmail.controls['helper_emails'];
        control.push(this.initHelpers());
    }

    handleSubmit(event) {
        event.preventDefault();
        let results = JSON.stringify(this.settingsEmail['value']);
        console.log('Form Saved', results);
        // TODO: save information to database
        this.closePage();
    }

    closePage() {
        this.events.publish('close:page');
    }

}
