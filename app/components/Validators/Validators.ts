import {Validators, FormBuilder, FormControl} from '@angular/forms';
var BadLanguageFilter = require('bad-language-filter');
var swears = new BadLanguageFilter();

interface Validation {
    [key: string]: boolean;
}

// Example - to repurpose outline
function isBanned(input: FormControl): Validation {
    if (input.value.toLowerCase() === 'banned-user') {
        // Is banned
        return {isBanned: true};
    }
    // All good
    return null;
}

interface Validator<T extends FormControl> {
    (c: T): {[error: string]: any};
}

export function validateEmail(c: FormControl) {
    let EMAIL_REGEXP = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;

    return EMAIL_REGEXP.test(c.value) ? null : {
        validateEmail: {
            valid: false
        }
    };
}

export function validateBadWords(c: FormControl) {
    console.log(swears);
    console.log("Checking for bad words!",c.value.toString());
    console.log("Bad words found? ",swears.contains("fuck"));

    return !swears.contains(c.value) ? null : {
        validateBadWords: {
            valid: true
        }
    }
}

export function sanitizeString(str){
    str = str.replace(/[^a-z0-9áéíóúñü \.,_-]/gim,"");
    return str.trim();
}
