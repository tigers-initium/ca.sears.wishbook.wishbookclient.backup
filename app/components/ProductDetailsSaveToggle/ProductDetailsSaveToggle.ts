import {Component} from '@angular/core';
import {IONIC_DIRECTIVES} from 'ionic-angular';
import {TranslatePipe} from 'ng2-translate';

@Component({
    selector: 'ProductDetailsSaveToggle',
    inputs: ['product'],
    pipes: [TranslatePipe],
    templateUrl: 'build/components/ProductDetailsSaveToggle/ProductDetailsSaveToggle.html',
    directives: [IONIC_DIRECTIVES] // makes all Ionic directives available to your component
})
export class ProductDetailsSaveToggle {
    public product: Object;
    public isSaved: boolean;

    constructor() {
        this.isSaved = false;
    }

    ngOnInit() {
        // Determine if product is saved or not
    }

}
