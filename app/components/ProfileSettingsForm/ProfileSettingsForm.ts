import {Component} from '@angular/core';
import {NavParams, Events} from 'ionic-angular';
import {Validators, FormBuilder, FormControl} from '@angular/forms';
import {validateEmail} from '../Validators/Validators';
import {TranslatePipe} from 'ng2-translate';

@Component({
    selector: 'ProfileSettingsForm',
    templateUrl: 'build/components/ProfileSettingsForm/ProfileSettingsForm.html',
    pipes: [TranslatePipe],
    inputs: ['profile']
})
export class ProfileSettingsFormComponent {

    signup: Object;
    profile: Object;

    constructor(private formBuilder: FormBuilder,
                public navParams: NavParams,
                public events: Events) {
    }

    ngOnInit() {
        this.signup = this.formBuilder.group({
            name: ['', [Validators.required, Validators.minLength(2)]],
            age: [''],
            gender: [''],
        });
    }

    handleSubmit(event) {
        event.preventDefault();
        let results = JSON.stringify(this.signup['value']);
        console.log("Updating profile",results);
    }

}
