import {Component} from '@angular/core';
import {IONIC_DIRECTIVES, Events} from 'ionic-angular';
import {TranslatePipe} from 'ng2-translate';

@Component({
    selector: 'ProductCard',
    pipes: [TranslatePipe],
    inputs: ['product', 'isSaved'],
    outputs: ['productDetailRequest'],
    templateUrl: 'build/components/ProductCard/ProductCard.html',
    directives: [IONIC_DIRECTIVES] // makes all Ionic directives available to your component
})
export class ProductCard {

    public product: Object;
    public isSaved: boolean;

    constructor(public events: Events) {
        this.subscribeToEvents();
    }

    subscribeToEvents() {
        this.events.subscribe('update:isSaved', (result) => {
            this.updateIsSaved(result[0]);
        });
    }

    select() {
        this.isSaved = true;
    }

    deselect() {
        this.isSaved = false;
    }

    toggleSelected(events) {
        let id = this.product['title'];

        // Optimistic UI - set saved status on click
        this.product['inWishlist'] = !this.product['inWishlist'];

        this.isSaved = !this.isSaved;

        event.stopPropagation();

        // Add to Wishlist
        if (this.isSaved) {
            this.events.publish('toggle:saveWishlistItem', id);
        } else {  // Remove from Wishlist
            this.events.publish('toggle:removeWishlistItem', id);
        }
    }

    updateIsSaved(data) {
        let id = data[0];

        let status = data[1];

        if (id === this.product['title']) {
            this.isSaved = status;

            this.product['inWishlist'] = status;
        }
    }
}
