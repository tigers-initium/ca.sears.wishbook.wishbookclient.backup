import {Component} from '@angular/core';
// import {IONIC_DIRECTIVES, NavParams} from 'ionic-angular';
// import {NavParams} from 'ionic-angular';

@Component({
    selector: 'ProfileIcon',
    templateUrl: 'build/components/ProfileIcon/ProfileIcon.html',
    inputs: ['profile']
})
export class ProfileIconComponent {

    public profile: Object;
    public bg_class: string;

    constructor() {
        this.bg_class = '';
    }

    ngOnInit() {

        if (!this.profile['photo'] || this.profile['photo'] === '') {

            let gender = this.profile['gender'];
            let gender_path = '';
            if (gender === 'M') {
                gender_path = 'boy';
            } else if (gender === 'F') {
                gender_path = 'girl';
            } else {
                gender_path = 'generic';
            }

            if (gender_path) {
                let photo = `img/icons/png/${gender_path}/default/${gender_path}@2x.png`;
                this.profile['photo'] = photo;
                this.bg_class = 'responsive-bg--fit';
            }
        }
    }

}
