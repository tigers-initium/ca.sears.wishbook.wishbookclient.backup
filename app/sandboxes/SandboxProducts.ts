import {Injectable} from '@angular/core';
import {Events, App} from 'ionic-angular';

// Custom pages
import {ListPage} from '../pages/list/list';
import {ProductDetailsPage} from '../pages/product-details/product-details';
import {SendPage} from '../pages/send/send';

@Injectable()
export class SandboxProducts {

    navControl: any;

    constructor(public events: Events,
                public app: App) {

        console.log('SANDBOX PRODUCTS');

        this.navControl = this.app.getActiveNav();
        this.subscribeToEvents();

    }

    subscribeToEvents() {
        // this.events.subscribe('db:finishLoadingDB', () => this.getAllProducts());
        this.events.subscribe('open:page', page => this.openPage(page[0]));

        this.events.subscribe('view:moreProducts', page => this.loadMoreProducts(page[0]));

        this.events.subscribe('get:moreProducts', () => this.requestMoreProducts());
    }

    getAllProducts(profileId) {
        let data = {
            profileId: profileId
        };
        this.events.publish('request:allProducts', data);
    }

    requestMoreProducts() {
        this.events.publish('request:moreProducts');
    }

    loadMoreProducts(page) {
        if (page.title === 'ListPage') {
            this.events.publish('load:moreProducts', page);
        }
    }

    openPage(page) {
        if (page) {

            switch (page.title) {
                case 'ProductDetailsPage':
                    this.showProductDetailsPage(page.product, page.wishlist);
                    break;
                case 'ListPage':
                    this.showListPage(page);
                    break;
                case 'ListPageAllProducts':
                    this.getAllProducts(page.profileId);
                    break;
                case 'SendPage':
                    this.showSendPage(page.products);
                    break;
            }

        }
    }

    showProductDetailsPage(product: any, wishlist: boolean) {

        let options = {};
        options['animate'] = true;
        options['animation'] = 'md-transition';
        options['duration'] = 400;

        this.navControl.push(ProductDetailsPage, {
            product: product,
            wishlist: wishlist,
        }, options);
    }

    showListPage(page) {
        this.navControl.push(ListPage, {
            page: page
        });
    }

    showSendPage(products) {
        this.generateWishList(products);
        this.navControl.push(SendPage);
    }

    generateWishList(products) {

        // Create a temporary wish list
        // TODO - generate email with full product info

        let list = '***WISHLIST*** \n';
        products.map(function (obj) {
            list += obj.name + ' \n';
        });

        // Write out the WISH LIST
        console.log(list);

    }

}
