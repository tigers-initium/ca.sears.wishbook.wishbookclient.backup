import {Injectable} from '@angular/core';
import {Events, App, MenuController} from 'ionic-angular';


// Custom Pages
import {SignupPage} from '../pages/signup/signup';
import {ProfilesPage} from '../pages/profiles/profiles';
import {SettingsPage} from '../pages/settings/settings';
import {ProfileSettingsPage} from '../pages/profile-settings/profile-settings';

@Injectable()
export class SandboxProfile {

    navControl: any;

    constructor(public events: Events,
                public app: App,
                public menuController: MenuController) {
        console.log('SANDBOX PROFILE');
        this.navControl = this.app.getActiveNav();
        this.subscribeToEvents();
        this.registerPageForMenu({title: 'Profiles', component: ProfilesPage});
        this.registerPageForMenu({title: 'Profile Settings', component: ProfileSettingsPage}); // TEMP
    }

    subscribeToEvents() {
        this.events.subscribe('open:page', page => this.openPage(page[0]));
        this.events.subscribe('request:createProfile', result => this.createProfile(result[0]));
        this.events.subscribe('allProfiles:viewed', result => this.showProfilesPage(result[0]));
        this.events.subscribe('new:profile', data => this.updateProfiles(data[0]));
    }

    registerPageForMenu(page: {title: string, component: Object}) {
        this.events.publish('request:pageRegister', page);
    }

    openPage(page) {
        if (page) {

            switch (page.title) {
                case 'root':
                case 'Profiles':
                    this.getAllProfiles();
                    // this.showPageFromMenu(page);
                    break;
                case 'Settings':
                    this.showPageFromMenu(page);
                    break;
                case 'SignupPage':
                    this.showSignupPage();
                    break;
                case 'Profile Settings':
                    this.showPageFromMenu(page);
                    break;
            }

        }
    }

    getAllProfiles() {
        this.events.publish('view:allProfiles');
    }

    updateProfiles(profile) {
        let data = {
            result: profile.results
        };
        if (profile.error) {
            console.error('SANDBOX PROFILES', profile.error);
            data = null;
        }
        this.events.publish('new:profileAdded', data);
    }

    showPageFromMenu(page) {
        this.navControl.push(page.component);
        this.menuController.close();
    }

    showSignupPage() {
        this.navControl.push(SignupPage);
    }

    showProfilesPage(profiles) {
        let profileData = {
            profiles: profiles.results
        };
        if (profiles.error) {
            console.error('SANDBOX PROFILES', profiles.error);
            profileData = null;
        }
        let options = {
            animate: true
        };
        console.log("Got profiles",profileData);
        this.navControl.setRoot(ProfilesPage, profileData, options);
        this.menuController.close();
    }

    createProfile(profileInfo) {
        let data = {
            profileInfo: profileInfo
        };

        this.events.publish('create:profile', data);

        this.events.publish('close:page');
    }

}
