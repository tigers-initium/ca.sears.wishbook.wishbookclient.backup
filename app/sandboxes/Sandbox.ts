import {Component} from '@angular/core';
import {Events, App, MenuController} from 'ionic-angular';

// Custom Injectables
import {CoreMenuBuilder} from '../Core/CoreMenuBuilder';
import {CoreDatabaseService} from '../Core/CoreDatabaseServices/CoreDatabaseService';
import {CoreProductsDBService} from '../Core/CoreDatabaseServices/CoreProductsDBService';
import {CoreWishlistsDBService} from '../Core/CoreDatabaseServices/CoreWishlistsDBService';
import {CoreProfilesDBService} from '../Core/CoreDatabaseServices/CoreProfilesDBService';
// import {ListPage} from '../pages/list/list';
// import {WishListPage} from '../pages/wish-list/wish-list';
// import {SendPage} from '../pages/send/send';
// import {SignupPage} from '../pages/signup/signup';
// import {ProfilesPage} from '../pages/profiles/profiles';

// Sandboxes
import {SandboxProducts} from './SandboxProducts';
import {SandboxProfile} from './SandboxProfile';
import {SandboxWishlist} from './SandboxWishlist';
import {SandboxOnboarding} from './SandboxOnboarding';

@Component({
    selector: 'Sandbox'
})
export class Sandbox {
    navControl: any;

    constructor(public events: Events,
                public app: App,
                public menuController: MenuController,
                public coreMenuBuilder: CoreMenuBuilder,
                public coreDatabaseService: CoreDatabaseService,
                public coreProductsDBService: CoreProductsDBService,
                public coreWishlistDBService: CoreWishlistsDBService,
                public coreProfileDBService: CoreProfilesDBService,
                public sandboxProducts: SandboxProducts,
                public sandboxProfile: SandboxProfile,
                public sandboxWishlist: SandboxWishlist,
                public sandboxOnboarding: SandboxOnboarding) {

        console.log('SANDBOX');
        this.navControl = this.app.getActiveNav();
        this.subscribeToEvents();

    }

    subscribeToEvents() {
        this.events.subscribe('open:page', page => this.openPage(page[0]));
        this.events.subscribe('close:page', () => this.closePage());
    }

    registerPageForMenu(page: {title: string, component: Object}) {
        this.events.publish('request:pageRegister', page);
    }

    openPage(page) {
        if (page) {
            switch (page.title) {
                case 'root':
                    this.showHomePage();
                    break;
            }

        }
    }

    showPageFromMenu(page) {
        console.log("Show Page From Menu");
        this.navControl.push(page.component);
        this.menuController.close();
    }

    closePage() {

        // Overriding iOS transition with Material
        // Breaks when closing Profile Signup page
        // TODO: Create separate modal close event
        let options = {};
        options['animate'] = true;
        options['animation'] = 'md-transition';
        options['duration'] = 250;

        this.navControl.pop();
    }

    showHomePage() {
        this.navControl.popToRoot();
    }
}
