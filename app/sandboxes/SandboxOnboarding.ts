import {Injectable} from '@angular/core';
import {Events, App, MenuController} from 'ionic-angular';

// Custom Pages
import {Onboarding} from '../pages/onboarding/onboarding';

@Injectable()
export class SandboxOnboarding {

    navControl: any;

    constructor(public app: App,
                public events: Events,
                public menuController: MenuController
    ) {
        console.log('SANDBOX ONBOARDING');
        this.navControl = this.app.getActiveNav();
        this.subscribeToEvents();
    }

    registerPageForMenu(page: {title: string, component: Object}) {
        this.events.publish('request:pageRegister', page);
    }

    subscribeToEvents() {
        this.events.subscribe('open:page', page => this.openPage(page[0]));
    }

    openPage(page) {
        if (page) {

            switch (page.title) {
            case 'Onboarding':
                this.showOnboarding(page);
                break;
            }

        }
    }

    showOnboarding(page) {
        let options = {
            animate: true
        };
        this.navControl.setRoot(Onboarding, null, options);
    }

}
