import {Injectable} from '@angular/core';
// import {Events} from 'ionic-angular';

const PouchDB = require('pouchdb');
PouchDB.plugin(require('pouchdb-upsert'));
PouchDB.plugin(require('pouchdb-find'));

@Injectable()
export class CoreProductsDBService {
    private db;

    // private productList: Array<{_id: any, _rev: any, name: any, url: any, type: any}>;

    private items;

    constructor() {
        // FOR TESTING ONLY
        this.items = [
            {
                _id: 'p_001',
                name: 'Captain America',
                url: 'img/action-figures/captainamerica.png',
                type: 'product'
            },
            {
                _id: 'p_002',
                name: 'R2D2',
                url: 'img/action-figures/r2d2.png',
                type: 'product'
            },
            {
                _id: 'p_003',
                name: 'Falcon',
                url: 'img/action-figures/falcon.png',
                type: 'product'
            },
            {
                _id: 'p_004',
                name: 'Spiderman',
                url: 'img/action-figures/spiderman.png',
                type: 'product'
            },
            {
                _id: 'p_005',
                name: 'Finn',
                url: 'img/action-figures/finn.png',
                type: 'product'
            },
            {
                _id: 'p_006',
                name: 'Star Wars Pack',
                url: 'img/action-figures/starwarspack.png',
                type: 'product'
            },
            {
                _id: 'p_007',
                name: 'Iron Man',
                url: 'img/action-figures/ironman.png',
                type: 'product'
            },
            {
                _id: 'p_008',
                name: 'Star Wars Pilot',
                url: 'img/action-figures/starwarspilot.png',
                type: 'product'
            },
            {
                _id: 'p_009',
                name: 'Jabba The Hut',
                url: 'img/action-figures/jabbathehut.png',
                type: 'product'
            },
            {
                _id: 'p_010',
                name: 'Storm Trooper',
                url: 'img/action-figures/stormtrooper.png',
                type: 'product'
            },
            {
                _id: 'p_011',
                name: 'Kylo Ren',
                url: 'img/action-figures/kyloren.png',
                type: 'product'
            },
            {
                _id: 'p_012',
                name: 'Titanium Series Vehicles',
                url: 'img/action-figures/titaniumseriesvehicles.png',
                type: 'product'
            },
            {
                _id: 'p_013',
                name: 'Minecraft',
                url: 'img/action-figures/minecraft.png',
                type: 'product'
            },
            {
                _id: 'p_014',
                name: 'Transformer',
                url: 'img/action-figures/transformer.png',
                type: 'product'
            },
            {
                _id: 'p_015',
                name: 'Minion',
                url: 'img/action-figures/minion.png',
                type: 'product'
            },
            {
                _id: 'p_016',
                name: 'T Rex',
                url: 'img/action-figures/trex.png',
                type: 'product'
            },
            {
                _id: 'p_017',
                name: 'Poe Dameron',
                url: 'img/action-figures/poedameron.png',
                type: 'product'
            },
            {
                _id: 'p_018',
                name: 'War Machine',
                url: 'img/action-figures/warmachine.png',
                type: 'product'
            },
            {
                _id: 'p_019',
                name: 'Quinjet',
                url: 'img/action-figures/quinjet.png',
                type: 'product'
            },
            {
                _id: 'p_020',
                name: 'Yoda Head',
                url: 'img/action-figures/yodahead.png',
                type: 'product'
            },
            {
                _id: 'p_021',
                name: 'Airport',
                url: 'img/blocks/airport.png',
                type: 'product'
            },
            {
                _id: 'p_022',
                name: 'Fortress Attack',
                url: 'img/blocks/fortressattack.png',
                type: 'product'
            },
            {
                _id: 'p_023',
                name: 'R2D2 Key',
                url: 'img/blocks/r2d2key.png',
                type: 'product'
            },
            {
                _id: 'p_024',
                name: 'Atlas Mobile Turret',
                url: 'img/blocks/atlasmobileturret.png',
                type: 'product'
            },
            {
                _id: 'p_025',
                name: 'Gun boat',
                url: 'img/blocks/gunboat.png',
                type: 'product'
            },
            {
                _id: 'p_026',
                name: 'Railway Deluxe',
                url: 'img/blocks/railwaydeluxe.png',
                type: 'product'
            },
            {
                _id: 'p_027',
                name: 'Banshee Strike',
                url: 'img/blocks/bansheestrike.png',
                type: 'product'
            },
            {
                _id: 'p_028',
                name: 'Horse Set',
                url: 'img/blocks/horseset.png',
                type: 'product'
            },
            {
                _id: 'p_029',
                name: 'Railway Set',
                url: 'img/blocks/railwayset.png',
                type: 'product'
            },
            {
                _id: 'p_030',
                name: 'Batman',
                url: 'img/blocks/batman.png',
                type: 'product'
            },
            {
                _id: 'p_031',
                name: 'Imperial Assault',
                url: 'img/blocks/imperialassault.png',
                type: 'product'
            },
            {
                _id: 'p_032',
                name: 'Roller Coaster',
                url: 'img/blocks/rollercoaster.png',
                type: 'product'
            },
            {
                _id: 'p_033',
                name: 'Booster Frame',
                url: 'img/blocks/boosterframe.png',
                type: 'product'
            },
            {
                _id: 'p_034',
                name: 'Kylo Ren key',
                url: 'img/blocks/kylorenkey.png',
                type: 'product'
            },
            {
                _id: 'p_035',
                name: 'Santa',
                url: 'img/blocks/santa.png',
                type: 'product'
            },
            {
                _id: 'p_036',
                name: 'Brio Roller Coaster',
                url: 'img/blocks/briorollercoaster.png',
                type: 'product'
            },
            {
                _id: 'p_037',
                name: 'Lakeside',
                url: 'img/blocks/lakeside.png',
                type: 'product'
            },
            {
                _id: 'p_038',
                name: 'Spider tank',
                url: 'img/blocks/spidertank.png',
                type: 'product'
            },
            {
                _id: 'p_039',
                name: 'Call of Duty',
                url: 'img/blocks/callofduty.png',
                type: 'product'
            },
            {
                _id: 'p_040',
                name: 'Medium Brick Box',
                url: 'img/blocks/mediumbrickbox.png',
                type: 'product'
            },
            {
                _id: 'p_041',
                name: 'Stormtrooper Key',
                url: 'img/blocks/stormtrooperkey.png',
                type: 'product'
            },
            {
                _id: 'p_042',
                name: 'Captain Key',
                url: 'img/blocks/captainkey.png',
                type: 'product'
            },
            {
                _id: 'p_043',
                name: 'Mega Blocks',
                url: 'img/blocks/megablocks.png',
                type: 'product'
            },
            {
                _id: 'p_044',
                name: 'Strike Fighter',
                url: 'img/blocks/strikefighter.png',
                type: 'product'
            },
            {
                _id: 'p_045',
                name: 'Castle',
                url: 'img/blocks/castle.png',
                type: 'product'
            },
            {
                _id: 'p_046',
                name: 'Modular Storage',
                url: 'img/blocks/modularstorage.png',
                type: 'product'
            },
            {
                _id: 'p_047',
                name: 'Superman',
                url: 'img/blocks/superman.png',
                type: 'product'
            },
            {
                _id: 'p_048',
                name: 'Tenkai',
                url: 'img/blocks/tenkai.png',
                type: 'product'
            },
            {
                _id: 'p_049',
                name: 'Chariot Chase',
                url: 'img/blocks/chariotchase.png',
                type: 'product'
            },
            {
                _id: 'p_050',
                name: 'Naval Cannon',
                url: 'img/blocks/navalcannon.png',
                type: 'product'
            },
            {
                _id: 'p_051',
                name: 'Tranzitdiner',
                url: 'img/blocks/tranzitdiner.png',
                type: 'product'
            },
            {
                _id: 'p_052',
                name: 'Darth Vader Keychain',
                url: 'img/blocks/darthvaderkeychain.png',
                type: 'product'
            },
            {
                _id: 'p_053',
                name: 'Patroller',
                url: 'img/blocks/patroller.png',
                type: 'product'
            },
            {
                _id: 'p_054',
                name: 'Travel Set',
                url: 'img/blocks/travelset.png',
                type: 'product'
            },
            {
                _id: 'p_055',
                name: 'Deluxe Railway',
                url: 'img/blocks/deluxerailway.png',
                type: 'product'
            },
            {
                _id: 'p_056',
                name: 'Poe Dameron Key',
                url: 'img/blocks/poedameronkey.png',
                type: 'product'
            },
            {
                _id: 'p_057',
                name: 'Tree house',
                url: 'img/blocks/treehouse.png',
                type: 'product'
            },
            {
                _id: 'p_058',
                name: 'Emmet',
                url: 'img/blocks/emmet.png',
                type: 'product'
            },
            {
                _id: 'p_059',
                name: 'Prison Island',
                url: 'img/blocks/prisonisland.png',
                type: 'product'
            },
            {
                _id: 'p_060',
                name: 'X-Wing Fighter',
                url: 'img/blocks/xwingfighter.png',
                type: 'product'
            },
            {
                _id: 'p_061',
                name: 'Barbie Agent',
                url: 'img/dolls/barbieagent.png',
                type: 'product'
            },
            {
                _id: 'p_062',
                name: 'Barbie Katniss',
                url: 'img/dolls/barbiekatniss.png',
                type: 'product'
            },
            {
                _id: 'p_063',
                name: 'Dream House',
                url: 'img/dolls/dreamhouse.png',
                type: 'product'
            },
            {
                _id: 'p_064',
                name: 'Elsa',
                url: 'img/dolls/elsa.png',
                type: 'product'
            },
            {
                _id: 'p_065',
                name: 'Barbie Ballet',
                url: 'img/dolls/barbieballet.png',
                type: 'product'
            },
            {
                _id: 'p_066',
                name: 'Barbie Mermaid',
                url: 'img/dolls/barbiemermaid.png',
                type: 'product'
            },
            {
                _id: 'p_067',
                name: 'Model Barbie',
                url: 'img/dolls/modelbarbie.png',
                type: 'product'
            },
            {
                _id: 'p_068',
                name: 'Barbie Birthday',
                url: 'img/dolls/barbiebirthday.png',
                type: 'product'
            },
            {
                _id: 'p_069',
                name: 'Barbie Peeta',
                url: 'img/dolls/barbiepeeta.png',
                type: 'product'
            },
            {
                _id: 'p_070',
                name: 'Olaf',
                url: 'img/dolls/olaf.png',
                type: 'product'
            },
            {
                _id: 'p_071',
                name: 'Barbie Cat',
                url: 'img/dolls/barbiecat.png',
                type: 'product'
            },
            {
                _id: 'p_072',
                name: 'Barbie Raquelle',
                url: 'img/dolls/barbieraquelle.png',
                type: 'product'
            },
            {
                _id: 'p_073',
                name: 'Playard',
                url: 'img/dolls/playard.png',
                type: 'product'
            },
            {
                _id: 'p_074',
                name: 'Barbie Dream House',
                url: 'img/dolls/barbiedreamhouse.png',
                type: 'product'
            },
            {
                _id: 'p_075',
                name: 'Barbie Rock',
                url: 'img/dolls/barbierock.png',
                type: 'product'
            },
            {
                _id: 'p_076',
                name: 'Walking Horse Barbie',
                url: 'img/dolls/walkinghorsebarbie.png',
                type: 'product'
            },
            {
                _id: 'p_077',
                name: 'Barbie Holiday 1',
                url: 'img/dolls/barbieholiday1.png',
                type: 'product'
            },
            {
                _id: 'p_078',
                name: 'Barbie Royal',
                url: 'img/dolls/barbieroyal.png',
                type: 'product'
            },
            {
                _id: 'p_079',
                name: 'Barbie Holiday 2',
                url: 'img/dolls/barbieholiday2.png',
                type: 'product'
            },
            {
                _id: 'p_080',
                name: 'Carriage Set',
                url: 'img/dolls/carriageset.png',
                type: 'product'
            },
            {
                _id: 'p_081',
                name: 'Aerial Drone',
                url: 'img/electronics/aerialdrone.png',
                type: 'product'
            },
            {
                _id: 'p_082',
                name: 'Kidizoom',
                url: 'img/electronics/kidizoom.png',
                type: 'product'
            },
            {
                _id: 'p_083',
                name: 'RC Quadcopter',
                url: 'img/electronics/rcquadcopter.png',
                type: 'product'
            },
            {
                _id: 'p_084',
                name: 'Air Hogs',
                url: 'img/electronics/airhogs.png',
                type: 'product'
            },
            {
                _id: 'p_085',
                name: 'Max Eye',
                url: 'img/electronics/maxeye.png',
                type: 'product'
            },
            {
                _id: 'p_086',
                name: 'Smallest Drone',
                url: 'img/electronics/smallestdrone.png',
                type: 'product'
            },
            {
                _id: 'p_087',
                name: 'Camera Quadcopter',
                url: 'img/electronics/cameraquadcopter.png',
                type: 'product'
            },
            {
                _id: 'p_088',
                name: 'Metal Drumset',
                url: 'img/electronics/metaldrumset.png',
                type: 'product'
            },
            {
                _id: 'p_089',
                name: 'Starship',
                url: 'img/electronics/starship.png',
                type: 'product'
            },
            {
                _id: 'p_090',
                name: 'Fly Eye',
                url: 'img/electronics/flyeye.png',
                type: 'product'
            },
            {
                _id: 'p_091',
                name: 'Micro Attack',
                url: 'img/electronics/microattack.png',
                type: 'product'
            },
            {
                _id: 'p_092',
                name: 'Video Drone',
                url: 'img/electronics/videodrone.png',
                type: 'product'
            },
            {
                _id: 'p_093',
                name: 'FPV Viewer',
                url: 'img/electronics/fpvviewer.png',
                type: 'product'
            },
            {
                _id: 'p_094',
                name: 'Propeller',
                url: 'img/electronics/propeller.png',
                type: 'product'
            },
            {
                _id: 'p_095',
                name: 'X-Drone',
                url: 'img/electronics/xdrone.png',
                type: 'product'
            },
            {
                _id: 'p_096',
                name: 'Grand Biker',
                url: 'img/electronics/grandbiker.png',
                type: 'product'
            },
            {
                _id: 'p_097',
                name: 'Quadcopter',
                url: 'img/electronics/quadcopter.png',
                type: 'product'
            },
            {
                _id: 'p_098',
                name: 'X-Wing Fighter',
                url: 'img/electronics/xwingfighter.png',
                type: 'product'
            },
            {
                _id: 'p_099',
                name: 'Gravity Pursuit',
                url: 'img/electronics/gravitypursuit.png',
                type: 'product'
            },
            {
                _id: 'p_100',
                name: 'Quad Force',
                url: 'img/electronics/quadforce.png',
                type: 'product'
            },
            {
                _id: 'p_101',
                name: 'Breach Blast',
                url: 'img/nerf-guns/breachblast.png',
                type: 'product'
            },
            {
                _id: 'p_102',
                name: 'Flip Fury',
                url: 'img/nerf-guns/flipfury.png',
                type: 'product'
            },
            {
                _id: 'p_103',
                name: 'Revolution Bow',
                url: 'img/nerf-guns/revolutionbow.png',
                type: 'product'
            },
            {
                _id: 'p_104',
                name: 'Torpedo Bow',
                url: 'img/nerf-guns/torpedobow.png',
                type: 'product'
            },
            {
                _id: 'p_105',
                name: 'Cross Bolt',
                url: 'img/nerf-guns/crossbolt.png',
                type: 'product'
            },
            {
                _id: 'p_106',
                name: 'Ion Fire',
                url: 'img/nerf-guns/ionfire.png',
                type: 'product'
            },
            {
                _id: 'p_107',
                name: 'Rival Zeus',
                url: 'img/nerf-guns/rivalzeus.png',
                type: 'product'
            },
            {
                _id: 'p_108',
                name: 'Triple Threat',
                url: 'img/nerf-guns/triplethreat.png',
                type: 'product'
            },
            {
                _id: 'p_109',
                name: 'Cross Cut',
                url: 'img/nerf-guns/crosscut.png',
                type: 'product'
            },
            {
                _id: 'p_110',
                name: 'Iron Man Slide Blast',
                url: 'img/nerf-guns/ironmanslideblast.png',
                type: 'product'
            },
            {
                _id: 'p_111',
                name: 'Rotofury',
                url: 'img/nerf-guns/rotofury.png',
                type: 'product'
            },
            {
                _id: 'p_112',
                name: 'Trooper Blaster',
                url: 'img/nerf-guns/trooperblaster.png',
                type: 'product'
            },
            {
                _id: 'p_113',
                name: 'Cyclone Shock',
                url: 'img/nerf-guns/cycloneshock.png',
                type: 'product'
            },
            {
                _id: 'p_114',
                name: 'Kryptonite Blaster',
                url: 'img/nerf-guns/kryptoniteblaster.png',
                type: 'product'
            },
            {
                _id: 'p_115',
                name: 'Sidekick Blaster',
                url: 'img/nerf-guns/sidekickblaster.png',
                type: 'product'
            },
            {
                _id: 'p_116',
                name: 'Zeus',
                url: 'img/nerf-guns/zeus.png',
                type: 'product'
            },
            {
                _id: 'p_117',
                name: 'ECS 10 Blaster',
                url: 'img/nerf-guns/ecs10blaster.png',
                type: 'product'
            },
            {
                _id: 'p_118',
                name: 'Reconmk2',
                url: 'img/nerf-guns/reconmk2.png',
                type: 'product'
            },
            {
                _id: 'p_119',
                name: 'Strike Dual',
                url: 'img/nerf-guns/strikedual.png',
                type: 'product'
            }
        ];

        // this.db.changes({live: true, since: 'now', include_docs: true})
        //   .on('change', (change) => {
        //     this.handleChange(change);
        //   });
    }

    public createDatabase() {
        return new Promise((resolve, reject) => {
            this.db = new PouchDB('ProductsDB');

            let promises = [];

            for (let item of this.items) {
                promises.push(this.db.putIfNotExists(item));
            }

            Promise.all(promises).then(() => {
                console.log('Done populating ProductsDB');

                resolve();
            }).catch((err) => {
                console.log(`ERROR createDatabase CoreProductsDBService: ${err}`);

                reject();
            });
        });
    }

    /**
     * Get the next n products.
     *
     * @param {object} options
     */
    public getProducts(options): Promise <Array<any>> {
        return new Promise((resolve, reject) => {
            this.db.allDocs(options)
                .then((result) => {
                    resolve(result['rows']);
                })
                .catch((err) => {
                    console.log('ERROR getProducts CoreProductsDBService:', err);

                    reject();
            });
        });
    }

    // /**
    //  * Return productList if it is already cached
    //  * Or get product list from database
    //  *
    //  * @param {array} itemIDs
    //  * @returns {Promise}
    //  */
    // public getAllProducts(itemIDs) {
    //     return new Promise((resolve, reject) => {
    //         this.productList = [];
    //
    //         this.db.find({
    //             selector: {
    //                 _id: {$gt: null}
    //             },
    //             sort: ['_id']
    //         }).then((result) => {
    //             let allProducts = result['docs'];
    //
    //             allProducts.map((row) => {
    //                 let productID = row._id;
    //
    //                 if (itemIDs.indexOf(productID) !== -1) {
    //                     row['inWishlist'] = true;
    //                 } else {
    //                     row['inWishlist'] = false;
    //                 }
    //
    //                 this.productList.push(row);
    //             });
    //
    //             resolve(this.productList);
    //         }).catch((err) => {
    //             console.log('ERROR Get All Products:', err);
    //
    //             reject();
    //         });
    //     });
    // }

    public addProduct(item) {
        return new Promise((resolve, reject) => {
            this.db.put(item)
                .then(() => {
                    resolve();
                })
                .catch((err) => {
                    console.log('ERROR addProduct:', err);

                    reject();
                });
        });
    }

    public updateProduct(id) {
        // TODO - get item before updating
        return new Promise((resolve, reject) => {
            this.getProductById(id).then((item) => {
                this.db.put(item)
                    .then(() => {
                       resolve();
                    });
            }).catch((err) => {
                console.log('ERROR updateProduct CoreProductsDBService:', err);

                reject();
            });
        });
    }

    public removeProduct(id) {
        return new Promise(function (resolve, reject) {
            this.getProductById(id).then((item) => {
                item['_deleted'] = true;

                this.db.put(item)
                    .then(function () {
                        resolve();
                    });
            }).catch(function (err) {
                console.log('ERROR updateProduct:', err.message);

                reject();
            });
        });
    }

    public getProductById(id) {
        return new Promise((resolve, reject) => {
            this.db.find({
                selector: {
                    _id: id,
                }
            }).then((result) => {
                resolve(result.docs[0]);
            }).catch((err) => {
                console.log('ERROR getProductById:', err);

                reject();
            });
        });
    }

    // private handleChange(change) {
    //     console.log('Changing database yo');
    //     let page = {
    //         title: 'ListPage',
    //         products: []
    //     };
    //
    //     if (this.productList) {
    //         let changedDoc = null;
    //         let changedIndex = null;
    //
    //         // Identify the changed doc
    //         this.productList.forEach((doc, index) => {
    //             if (doc._id === change.id) {
    //                 changedDoc = doc;
    //                 changedIndex = index;
    //             }
    //         });
    //
    //         // When a document is deleted
    //         if (change.deleted) {
    //             this.productList.splice(changedIndex, 1);
    //         } else {
    //             // When a document was updated
    //             if (changedDoc) {
    //                 this.productList[changedIndex] = change.doc;
    //             } else {  // When a document was added
    //                 this.productList.push(change.doc);
    //             }
    //         }
    //
    //         page.products = this.productList;
    //         this.events.publish('open:page', page);
    //         // this.events.publish('view:allProducts', this.productList);
    //     }
    // }
}
