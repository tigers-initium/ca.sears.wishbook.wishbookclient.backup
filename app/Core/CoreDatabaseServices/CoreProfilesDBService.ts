/**
 * Created by mohamed on 19/09/16.
 */
import {Injectable} from '@angular/core';
import {Events} from 'ionic-angular';

const PouchDB = require('pouchdb');
PouchDB.plugin(require('pouchdb-upsert'));
PouchDB.plugin(require('pouchdb-find'));

@Injectable()
export class CoreProfilesDBService {
    private db;
    private data = {
        results: {},
        error: null
    };

    constructor(public events: Events) {
        console.log('CORE PROFILE DB SERVICE');
    }

    // private subscribeToEvents() {
        // this.events.subscribe('create:profile', (data) => {
        //     let profileInfo = JSON.parse(data[0]);
        //     this.createProfile(profileInfo);
        // });
        // this.events.subscribe('view:profile', (data) => {
        //     this.viewProfile(data[0].coreDbService, data[0].profileId, data[0].options);
        // });
        // this.events.subscribe('view:allProfiles', () => {
        //     let option = null;
        //
        //     this.viewAllProfiles(option);
        // });
        // this.events.subscribe('update:addProductToProfile', (data) => {
        //     this.addProductToProfile(data[0].coreDbService, data[0].profileId, data[2].productId, data[0].options);
        // });
        // this.events.subscribe('update:removeProductToProfile', (data) => {
        //     this.removeProductToProfile(data[0].coreDbService, data[0].profileId, data[0].productId, data[0].options);
        // });
        // this.events.subscribe('delete:profile', (data) => {
        //     this.deleteProfile(data[0].coreDbService, data[0].profileId, data[0].options);
        // });
    // }

    public createDatabase() {
        return new Promise ((resolve) => {
            this.db = new PouchDB('ProfilesDB');

            resolve();

            // let promises = [];
            //
            // for (let item of this.items) {
            //     promises.push(this.db.putIfNotExists(item));
            // }
            //
            // Promise.all(promises).then(() => {
            //     console.log('Done populating ProfilesDB');
            //
            //     resolve();
            // }).catch((err) => {
            //     console.log(`ERROR createDatabase CoreProfilesDBService: ${err}`);
            //
            //     reject();
            // });
        });
    }

    createProfile(profileInfo): Promise<{ _id }> {
        return new Promise ((resolve, reject) => {
            this.db.post(
                profileInfo
            ).then((response) => {
                this.db.get(response.id).then((profile) => {
                    this.data.results = profile;
                    this.events.publish('new:profile', this.data);
                    resolve(profile);
                });
            }).catch((error) => {
                console.error(`ERROR createProfile CoreProfilesDBServices: ${error}`);
                reject();
            });
        });
    }

    viewProfile(profileId, options = null) {
        options = options || {};
        this.db.get(
            profileId,
            options
        ).then((profile) => {
            this.data.results = profile;
            this.events.publish('profile:viewed', this.data);
        }).catch((error) => {
            this.data.error = error;
            this.events.publish('profile:viewed', this.data);
        });
    }

    viewAllProfiles(options = null) {
        options = options || { include_docs: true };
        this.db.allDocs(
            options
        ).then(results => {
            this.data.results = results;
            this.events.publish('allProfiles:viewed', this.data);
        }).catch(error => {
            this.data.error = error;
            this.events.publish('allProfiles:viewed', this.data);
        });
    }

    addProductToProfile(profileId, productId, options = null) {
        options = options || {};
        this.db.get(
            profileId,
            options
        ).then((profile) => {
            profile.products.push(productId);
            this.db.put(
                profile
            ).then((newProfile) => {
                this.data.results = newProfile;
                this.events.publish('addProductToProfile:updated', this.data);
            }).catch((error) => {
                this.data.error = error;
                this.events.publish('addProductToProfile:updated', this.data);
            });
        }).then((error) => {
            this.data.error = error;
            this.events.publish('addProductToProfile:updated', this.data);
        });
    }

    removeProductToProfile(profileId, productId, options = null) {
        options = options || {};
        this.db.get(
            profileId,
            options
        ).then((profile) => {
            let productIndex = profile.products.indexOf(productId);
            if (productIndex !== -1) {
                profile.products.splice(productIndex, 1);
                this.db.put(
                    profile
                ).then((newProfile) => {
                    this.data.results = newProfile;
                    this.events.publish('removeProductToProfile:updated', this.data);
                }).catch((error) => {
                    this.data.error = error;
                    this.events.publish('removeProductToProfile:updated', this.data);
                });
            } else {
                this.events.publish('removeProductToProfile:updated', 'Product not found');
            }
        }).then((error) => {
            this.data.error = error;
            this.events.publish('removeProductToProfile:updated', this.data);
        });
    }

    deleteProfile(profileId, options = null) {
        options = options || {};
        this.db.get(
            profileId,
            options
        ).then((profile) => {
            profile._deleted = true;
            this.db.put(
                profile
            ).then((newProfile) => {
                this.data.results = newProfile;
                this.events.publish('profile:deleted', this.data);
            }).catch((error) => {
                this.data.error = error;
                this.events.publish('profile:deleted', this.data);
            });
        }).then((error) => {
            this.data.error = error;
            this.events.publish('profile:deleted', this.data);
        });
    }
}
