import {Injectable} from '@angular/core';

const PouchDB = require('pouchdb');
PouchDB.plugin(require('pouchdb-upsert'));
PouchDB.plugin(require('pouchdb-find'));

@Injectable()
export class CoreWishlistsDBService {
    private db;

    private remoteDB;

    constructor() {
        // this.db.changes({live: true, since: 'now', include_docs: true})
        //   .on('change', (change) => {
        //     this.handleChange(change);
        //   });
    }

    public createDatabase() {
        return new Promise ((resolve) => {
            this.db = new PouchDB('WishlistsDB');

            this.remoteDB = new PouchDB('');

            resolve();
            // let promises = [];
            //
            // for (let item of this.items) {
            //     promises.push(this.db.putIfNotExists(item));
            // }
            //
            // Promise.all(promises).then(() => {
            //     console.log('Done populating WishlistsDB');
            //
            //     resolve();
            // }).catch((err) => {
            //     console.log(`ERROR createDatabase CoreWishlistsDBService: ${err}`);
            //
            //     reject();
            // });
        });
    }

    /**
     * Get items in wishlist with ID id.
     * Items in wishlist are product Ids.
     *
     * @param string id
     * @param array result
     */
    public getWishlistItems(id): Promise <Array<any>> {
        return new Promise ((resolve, reject) => {
            let productIds = [];

            this.getWishlistById(id).then((wishlist) => {
                if (wishlist) {
                    productIds = wishlist['items'];

                    resolve(productIds);
                }
            }).catch((err) => {
                console.log('ERROR getWishlistItems ProductDB:', err);

                reject();
            });
        });
    }

    /**
     * Get Wish list FROM DATABASE by id
     *
     * @param id
     */
    public getWishlistById(id) {
        return new Promise((resolve, reject) => {
            this.db.get(id).then((result) => {
                resolve(result);
            }).catch((err) => {
                console.log('ERROR get Wishlist by Id:', err);

                reject();
            });
        });
    }

    /**
     * Add an productID to wishlist with ID wishlistId
     *
     * @param string wishlistId
     * @param string productId
     */
    public addWishlistItem(wishlistId, productId) {
        return new Promise ((resolve, reject) => {
            this.getWishlistById(wishlistId).then((wishlist) => {

                wishlist['items'].push(productId);

                this.db.put(wishlist)
                    .then(() => {
                        console.log('Item added to Wishlist', productId);

                        resolve();
                    })
                    .catch((err) => {
                        console.log('ERROR add to wishlist:', err);

                        reject();
                    });
            });
        });
    }

    public removeWishlistItem(wishlistId, productId) {
        return new Promise ((resolve, reject) => {
            this.getWishlistById(wishlistId).then((wishlist) => {

                let index = wishlist['items'].indexOf(productId, 0);

                if (index > -1) {
                    wishlist['items'].splice(index, 1);
                }

                this.db.put(wishlist)
                    .then(() => {
                        console.log('Item removed from Wishlist', productId);

                        resolve();
                    })
                    .catch((err) => {
                        console.log('ERROR add to wishlist:', err);

                        reject();
                    });
            });
        });
    }

    public addWishlist(id) {
        return new Promise((resolve, reject) => {
            let item = {
                _id: id,
                items: []
            };
            this.db.put(item)
                .then(() => {
                    resolve();
                })
                .catch((err) => {
                    console.log(`ERROR addWishlist: ${err.message}`);

                    reject();
                });
        });
    }

    public removeWishlist(id) {
        return new Promise((resolve, reject) => {
            this.getWishlistById(id)
                .then((item) => {
                    item['_deleted'] = true;

                    this.db.put(item)
                        .then(() => {
                           resolve();
                        });
                }).catch((err) => {
                    console.log('ERROR removeWishlist CoreWishlistsDBService:', err);

                    reject();
            });
        });
    }

//     private handleChange(change) {
//
//     }
}
