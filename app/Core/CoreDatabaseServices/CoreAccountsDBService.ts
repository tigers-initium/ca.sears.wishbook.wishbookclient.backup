import {Injectable} from '@angular/core';

const PouchDB = require('pouchdb');
PouchDB.plugin(require('pouchdb-upsert'));
PouchDB.plugin(require('pouchdb-find'));

@Injectable()
export class CoreAccountsDBService {
    private db;

    private items;

    constructor() {
        // FOR TESTING ONLY.
        this.items = [
            {
                _id: 'a_1',
                name: 'Hermione Granger',
                email: 'h.granger@spew.uk',
                password: 'book',
                profiles: [],
                type: 'account'
            }
        ];
    }

    public createDatabase() {
        return new Promise ((resolve, reject) => {
            this.db = new PouchDB('AccountsDB');

            let promises = [];

            for (let item of this.items) {
                promises.push(this.db.putIfNotExists(item));
            }

            Promise.all(promises).then(() => {
                console.log('Done populating AccountsDB');

                resolve();
            }).catch((err) => {
                console.log(`ERROR createDatabase CoreAccountsDBService: ${err}`);

                reject();
            });
        });
    }

    public getAccountById(id) {
        return new Promise((resolve, reject) => {
            this.db.find({
                selector: {
                    _id: id,
                }
            }).then((result) => {
                resolve(result);
            }).catch((err) => {
                console.log('ERROR getAccountById AccountDB:', err);

                reject();
            });
        });
    }

    public authenticate(id, email, password) {
        return new Promise((resolve, reject) => {
            this.getAccountById(id)
                .then((account) => {
                    if (account['email'] === email && account['password'] === password) {
                        resolve(true);
                    } else {
                        resolve(false);
                    }
                }).catch((err) => {
                console.log(`ERROR authenticate AccountDB: ${err}`);

                reject();
            });
        });
    }

    public updateName(id, name) {
        return new Promise((resolve, reject) => {
            this.getAccountById(id).then((account) => {
                account['name'] = name;

                this.db.put(account)
                    .then(() => {
                        resolve();
                    }).catch((err) => {
                    console.log(`ERROR updateName AccountDB: ${err}`);

                    reject();
                });
            });
        });
    }

    public updatePassword(id, email, password) {
        return new Promise((resolve, reject) => {
            let promises = [];

            promises.push(this.authenticate(id, email, password));

            promises.push(this.getAccountById(id));

            Promise.all(promises).then((result) => {
                let account = result[1];

                if (result[0] && account) {
                    account['password'] = password;

                    this.db.put(account)
                        .then(() => {
                            resolve(true);
                        });
                } else {
                    // TODO - Alert password and email incorrect
                    resolve(false);
                }
            }).catch((err) => {
                console.log(`ERROR updatePassword AccountDB ${err}`);
            });
        });
    }

    /**
     * Add a new account.
     * User needs to connect to Internet.
     * Pull data and push change from/to remote server directly.
     */
    public addNew() {

    }

    public remove(id) {

    }
}
