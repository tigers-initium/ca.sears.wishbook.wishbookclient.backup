/**
 * Run command add-cors-to-couchdb if run into CORS issue
 */
import {Injectable} from '@angular/core';
import {Events} from 'ionic-angular';

// Database Services
import { CoreProductsDBService } from './CoreProductsDBService';
import { CoreWishlistsDBService } from './CoreWishlistsDBService';
import { CoreAccountsDBService } from './CoreAccountsDBService';
import { CoreProfilesDBService } from './CoreProfilesDBService';

@Injectable()
export class CoreDatabaseService {
    // private remote: string;

    profileId: any;

    listOptions: any;

    wishlistOptions: any;

    wishlistIndex: any;

    constructor(
        public events: Events,
        public productsDB: CoreProductsDBService,
        public wishlistsDB: CoreWishlistsDBService,
        public accountsDB: CoreAccountsDBService,
        public profilesDB: CoreProfilesDBService
    ) {
        console.log('CORE DATABASE SERVICE');

        // // Sync with CouchDB server Database
        // this.remote = 'http://localhost:5984/_utils/database.html?testwishserver';
        //
        // let options = {
        //   live: true,
        //   retry: true,
        //   continuous: true
        // };
        //
        // this.db.sync(this.remote, options)
        //   .on('error', (err) => {
        //     console.log("ERROR syncing database", err);
        //   })
        //   .on('complete', () => {
        //     console.log("Done Syncing Database");
        //
        //     this.db.info().then(() => { console.log.bind(console.log);});
        //
        //     this.events.publish('db:finishLoadingDB');
        // });

        // this.db.changes({live: true, since: 'now', include_docs: true})
        //   .on('change', (change) => {
        //     this.handleChange(change);
        //   });

        // }


        // FOR TESTING ONLY. Create all databases
        let promises = [];

        promises.push(this.productsDB.createDatabase());

        promises.push(this.wishlistsDB.createDatabase());

        promises.push(this.accountsDB.createDatabase());

        promises.push(this.profilesDB.createDatabase());

        Promise.all(promises).then(() => {
            console.log('Done populating databases');
            // this.events.publish('db:finishLoadingDB');
        }).catch(function (err) {
            console.log('ERROR Populating DB:', err.message);
        });

        // Subscribe to events
        this.subscribeToProductEvents();

        this.subscribeToWishlistEvents();

        this.subscribeToProfileEvents();

        this.subscribeToAccountEvents();
    }

    /**
     * Subscribe to event that requests all products
     */
    private subscribeToProductEvents() {
        this.events.subscribe('request:allProducts', data => {
            this.listOptions = {
                include_docs: true,
                limit: 20
            };

            this.profileId = data[0].profileId;

            this.getListPageProducts(this.profileId)
                .then((products) => {
                    this.sendProducts(products, 'ListPage', true);
                });

        });

        this.events.subscribe('request:moreProducts', () => {
            this.getListPageProducts(this.profileId)
                .then((products) => {
                    this.sendProducts(products, 'ListPage', false);
                });
        });
    }

    /**
     * Subscribe to all events related to Wishlist
     */
    private subscribeToWishlistEvents() {
        this.events.subscribe('request:wishlist', () => {
            this.wishlistOptions = {
                include_docs: true,
                limit: 20
            };

            this.wishlistIndex = 0;

            this.getWishlistProducts(this.profileId)
                .then((products) => {
                    this.sendProducts(products, 'WishListPage', true);
                });
        });

        this.events.subscribe('request:moreWishlistProducts', () => {
            this.getWishlistProducts(this.profileId)
                .then((products) => {
                    this.sendProducts(products, 'WishListPage', false);
                });
        });

        this.events.subscribe('save:wishlistItem', (data) => {
            this.saveWishlistItem(this.profileId, data[0]); // TODO - need to use id saved in local storage instead
        });

        this.events.subscribe('remove:wishlistItem', (data) => {
            this.removeWishlistItem(this.profileId, data[0]); // TODO - need to use id saved in local storage instead
        });
    }

    /**
     * Subscribe to all events related to Profiles
     */
    private subscribeToProfileEvents() {
        this.events.subscribe('create:profile', (data) => {
            this.setupNewProfile(data);
        });

        this.events.subscribe('view:profile', (data) => {
           this.profilesDB.viewProfile(data[0].profileId, data[0].options);
        });

        this.events.subscribe('view:allProfiles', () => {
            this.profilesDB.viewAllProfiles();
        });

        this.events.subscribe('update:addProductToProfile', (data) => {
            this.profilesDB.addProductToProfile(data[0].profileId, data[2].productId, data[0].options);
        });

        this.events.subscribe('update:removeProductToProfile', (data) => {
            this.profilesDB.removeProductToProfile(data[0].profileId, data[0].productId, data[0].options);
        });

        this.events.subscribe('delete:profile', (data) => {
            this.profilesDB.deleteProfile(data[0].profileId, data[0].options);
        });
    }

    /**
     * Subscribe to all events related to Accounts
     */
    private subscribeToAccountEvents() {

    }

    private setupNewProfile(data) {
        let profileInfo = JSON.parse(data[0].profileInfo);

        this.profilesDB.createProfile(profileInfo)
            .then((profile) => {
               this.wishlistsDB.addWishlist(profile._id)
                   .then(() => {
                       this.events.publish('profile:created');
                   });
            })
            .catch((err) => {
                console.log('ERROR setupNewProfile CoreDatabaseServices:', err);
            });
    }

    /**
     * Query ProductsDB for the next n products.
     * n = the limit number in variables options above.
     * If forWishlist is true, get wishlist items.
     *
     * @param {array} itemIDs
     * @param {object} options
     */
    private getProducts(itemIDs, options) {
        let productList = [];

        return new Promise((resolve, reject) => {
            this.productsDB.getProducts(options)
                .then((result) => {
                    if (result && result.length > 0 && options.keys === undefined) {
                        let startProduct = result[result.length - 1];

                        options.startkey = startProduct.id;

                        options.skip = 1;
                    }

                    result.map((row) => {
                        let product = row.doc;

                        let productID = product._id;

                        if (itemIDs.indexOf(productID) !== -1 ) {
                            product['inWishlist'] = true;
                        } else {
                            product['inWishlist'] = false;
                        }

                        productList.push(product);
                    });

                    resolve(productList);
                })
                .catch((err) => {
                    console.log('ERROR getProducts CoreDatabaseService:', err);

                    reject();
                });
        });
    }

    private getListPageProducts(id) {
        return new Promise((resolve, reject) => {
            this.wishlistsDB.getWishlistItems(id)
                .then((itemIDs) => {
                    this.getProducts(itemIDs, this.listOptions)
                        .then((products) => {
                            resolve(products);
                        });
                })
                .catch((err) => {
                    console.log('ERROR getListPageProducts CoreDatabaseService:', err);

                    reject();
                });
        });
    }

    private getWishlistProducts(id): Promise <Array<any>> {
        return new Promise((resolve, reject) => {
            this.wishlistsDB.getWishlistItems(id)
                .then((result) => {
                    let endIndex = this.wishlistIndex + this.wishlistOptions['limit'];

                    let itemIDs = result.slice(this.wishlistIndex, endIndex);

                    this.wishlistIndex = endIndex;

                    this.wishlistOptions['keys'] = itemIDs;

                    this.getProducts(itemIDs, this.wishlistOptions)
                        .then((products) => {
                            resolve(products);
                        });
                })
                .catch((err) => {
                    console.log('ERROR getListPageProducts CoreDatabaseService:', err);

                    reject();
                });
        });
    }

    // /**
    //  * Query CoreWishlistsDBService for wishlist items
    //  * Then query CoreProductsDBService for all products.
    //  * Resolved values are all products with wishlist items marked.
    //  *
    //  * @param id
    //  * @returns {Promise<T>}
    //  */
    // private getAllProducts(id) {
    //     return new Promise((resolve, reject) => {
    //         this.wishlistsDB.getWishlistItems(id)
    //             .then((items) => {
    //                 this.productsDB.getAllProducts(items)
    //                     .then((products) => {
    //                         resolve(products);
    //                     });
    //             }).catch((err) => {
    //             console.log(`ERROR getAllProducts CoreDatabaseService ${err}`);
    //
    //             reject();
    //         });
    //     });
    // }

    // private getWishlistProducts(id): Promise <Array<any>> {
    //     return new Promise((resolve, reject) => {
    //         this.wishlistsDB.getWishlistItems(id)
    //             .then((items) => {
    //                 let promises = [];
    //
    //                 for (let itemID of items) {
    //                     promises.push(this.productsDB.getProductById(itemID));
    //                 }
    //
    //                 Promise.all(promises).then((products) => {
    //                     resolve(products);
    //                 });
    //             }).catch((err) => {
    //             console.log(`ERROR getWishlistProducts CoreDatabaseService ${err}`);
    //
    //             reject();
    //         });
    //     });
    // }

    private saveWishlistItem(wishlistId, productId) {
        this.wishlistsDB.addWishlistItem(wishlistId, productId)
            .then(() => {
                this.events.publish('view:wishlistChanged', [productId, true]);
            });
    }

    private removeWishlistItem(wishlistId, productId) {
        this.wishlistsDB.removeWishlistItem(wishlistId, productId)
            .then(() => {
                this.events.publish('view:wishlistChanged', [productId, false]);
            });
    }

    private sendProducts(products, title, firstLoad) {
        let page = {
            title: title,
            firstLoad: firstLoad,
            products: []
        };

        page.products = products;

        if (firstLoad) {
            this.openPage(page);
        } else {
            this.sendMore(page);
        }
    }

    private openPage(page) {
        this.events.publish('open:page', page);
    }

    private sendMore(page) {
        if (page.title === 'ListPage') {
            this.events.publish('view:moreProducts', page);
        } else {
            this.events.publish('view:moreWishlistProducts', page);
        }
    }

    // TODO - handle change in each database
}
