/**
 * Created by mohamed on 15/09/16.
 */

import {Component} from '@angular/core';
import {Events} from 'ionic-angular';

@Component({
    selector: 'CoreMenuBuilder'
})
export class CoreMenuBuilder {

    constructor(public events: Events) {
        console.log('CORE MENU BUILDER');
        this.subscribeToEvents();
    }

    subscribeToEvents() {
        this.events.subscribe('request:pageRegister', (page) => {
            this.addPageToMenu(page);
        });
    }

    addPageToMenu(page: any) {
        this.events.publish('add:menuEntry', page);
    }

}
