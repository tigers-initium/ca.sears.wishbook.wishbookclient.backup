import {Component} from '@angular/core';
import {Http} from '@angular/http';
import {StatusBar} from 'ionic-native';
import {
    ionicBootstrap,
    Platform,
    Events
} from 'ionic-angular';
import {
    TranslateService,
    TranslatePipe,
    TranslateLoader,
    TranslateStaticLoader
} from 'ng2-translate/ng2-translate';

// Custom Injectables
import {CoreDatabaseService} from './Core/CoreDatabaseServices/CoreDatabaseService';
import {CoreProductsDBService} from './Core/CoreDatabaseServices/CoreProductsDBService';
import {CoreWishlistsDBService} from './Core/CoreDatabaseServices/CoreWishlistsDBService';
import {CoreProfilesDBService} from './Core/CoreDatabaseServices/CoreProfilesDBService';
import {CoreAccountsDBService} from './Core/CoreDatabaseServices/CoreAccountsDBService';
// import {CoreCommunicator} from './Core/CoreCommunicator';
import {CoreMenuBuilder} from './Core/CoreMenuBuilder';

// Custom Pages
import {LoadingPage} from './pages/loading/loading';

// Sandboxes
import {Sandbox} from './sandboxes/Sandbox';
import {SandboxProducts} from './sandboxes/SandboxProducts';
import {SandboxProfile} from './sandboxes/SandboxProfile';
import {SandboxWishlist} from './sandboxes/SandboxWishlist';
import {SandboxOnboarding} from './sandboxes/SandboxOnboarding';

// Form overrides - to remove once Ionic updates Angular version
import {disableDeprecatedForms, provideForms} from '@angular/forms';

@Component({
    templateUrl: 'build/app.html',
    pipes: [TranslatePipe],
    providers: [
        {
            provide: TranslateLoader,
            useFactory: (http: Http) => new TranslateStaticLoader(http, 'assets/i18n', '.json'),
            deps: [Http]
        },
        TranslateService
    ]
})
export class MyApp {

    rootPage: any = LoadingPage;
    platform: Platform = new Platform;
    pages: Array<{title: string, component: Object}> = [];

    constructor(public events: Events,
                public translateService: TranslateService,
                public http: Http) {
        console.log('MY APP');
        this.initializeApp();
        this.subscribeToEvents();
        this.initializeTranslateService();
    }

    subscribeToEvents() {
        this.events.subscribe('add:menuEntry', (page) => {
            this.addMenuEntry(page[0][0]);
        });
    }

    initializeTranslateService() {
        let userLang = navigator.language.split('-')[0];
        userLang = /(en|fr)/gi.test(userLang) ? userLang : 'en';

        this.translateService.setDefaultLang('en');
        this.translateService.use(userLang);
    }

    openPage(page) {
        this.events.publish('open:page', page);
    }

    addMenuEntry(page) {
        this.pages.push(page);
    }

    initializeApp() {
        this.platform.ready().then(() => {
            StatusBar.styleDefault();
        });
    }
}

ionicBootstrap(MyApp, [
    CoreDatabaseService,
    CoreProductsDBService,
    CoreWishlistsDBService,
    CoreProfilesDBService,
    CoreAccountsDBService,
    CoreMenuBuilder,
    Sandbox,
    SandboxProducts,
    SandboxProfile,
    SandboxWishlist,
    SandboxOnboarding,
    disableDeprecatedForms(),
    provideForms()
]);
