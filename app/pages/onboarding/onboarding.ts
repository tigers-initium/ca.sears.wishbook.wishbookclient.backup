import {Component, ViewChild} from '@angular/core';
import {TranslatePipe} from 'ng2-translate';
import {Sandbox} from '../../sandboxes/Sandbox';
import {IONIC_DIRECTIVES, NavParams, Events, Slides} from 'ionic-angular';
import {OnboardingProfileComponent} from '../../components/OnboardingProfile/OnboardingProfile';
import {OnboardingEmailComponent} from '../../components/OnboardingEmail/OnboardingEmail';
import {OnboardingListComponent} from '../../components/OnboardingList/OnboardingList';
import {OnboardingCheckComponent} from '../../components/OnboardingCheck/OnboardingCheck';

@Component({
    templateUrl: 'build/pages/onboarding/onboarding.html',
    pipes: [TranslatePipe],
    directives: [IONIC_DIRECTIVES, OnboardingProfileComponent, OnboardingEmailComponent, OnboardingListComponent, OnboardingCheckComponent]
})
export class Onboarding {

    complete: boolean;
    exiting: boolean;

    @ViewChild('OnboardingSlider') slider: Slides;

    constructor(public navParams: NavParams,
                public events: Events) {
        this.complete = false;
        this.exiting = false;
    }

    showProfilesPage() {
        let page = {
            title: 'Profiles'
        };

        this.events.publish('open:page', page);
    }

    slideNext() {
        this.slider.slideNext();
    }

    onSlideChanged() {
        const max = this.slider.length();
        let currentIndex = this.slider.getActiveIndex();
        if (currentIndex === (max - 1)) {
            this.complete = true;
        }
    }

    ionViewWillLeave() {
        this.exiting = true;
    }

}
