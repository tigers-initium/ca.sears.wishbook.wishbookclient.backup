import {Component} from '@angular/core';
import {NavParams, Events} from 'ionic-angular';
import {TranslatePipe} from 'ng2-translate';
import {ProfileIconComponent} from '../../components/ProfileIcon/ProfileIcon';

@Component({
    templateUrl: 'build/pages/profiles/profiles.html',
    directives: [ProfileIconComponent],
    pipes: [TranslatePipe]
})
export class ProfilesPage {

    profiles: any;

    constructor(public navParams: NavParams,
                public events: Events) {
        this.profiles = navParams.get('profiles');
        if (this.profiles == null || this.profiles instanceof Error) {
            this.profiles = [];
        } else {
            this.profiles = this.profiles.rows;
        }
        this.subscribeToEvents();
    }

    subscribeToEvents() {
        this.events.subscribe('new:profileAdded', data => this.updateProfiles(data[0]));
    }

    showAllProfiles() {
        console.log('Profiles - show all profiles');
        console.log(this.navParams);
    }

    showProfilePage() {

    }

    updateProfiles(profile) {
        let newProfile = {
            id: profile.result._id,
            rev: profile.result._rev,
            doc: {
                name: profile.result.name,
                age: profile.result.age,
                email: profile.result.email,
                gender: profile.result.gender,
                photo: profile.result.photo
            }
        };
        this.profiles.unshift(newProfile);
    }

    showListPage(profile) {
        let page = {
            title: 'ListPageAllProducts',
            profileId: profile.id
        };
        this.events.publish('open:page', page);
    }

    showSignupPage() {
        let page = {
            title: 'SignupPage'
        };

        this.events.publish('open:page', page);
    }


}
