import {Component} from '@angular/core';
import {Events, NavParams} from 'ionic-angular';
import {TranslatePipe} from 'ng2-translate';
import {ProductCard} from '../../components/ProductCard/ProductCard.ts';

@Component({
    templateUrl: 'build/pages/wish-list/wish-list.html',
    directives: [ProductCard],
    pipes: [TranslatePipe]
})
export class WishListPage {
    selectedProduct: any;
    products: Array<{title: string, name: string, url: string, inWishlist: boolean}>;
    isReadyToSubmit: boolean;
    onscreen: boolean;
    infiniteScroll: any;

    constructor(public events: Events,
                public navParams: NavParams) {

        this.isReadyToSubmit = true;
        this.products = [];
        this.onscreen = false;

        this.showWishlist(navParams.get('page'));

        this.subscribeToEvents();
    }

    subscribeToEvents() {
        this.events.subscribe('load:moreWishlistProducts', (page) => {
            this.showWishlist(page[0]);
        });
    }

    showWishlist(page) {
        let items = page.products;

        if (page.firstLoad) {
            this.products = [];
        }

        for (let item of items) {
            this.products.push({
                title: item._id,
                name: item.name,
                url: item.url,
                inWishlist: item.inWishlist
            });
        }

        this.stopScrolling();
    }

    showProductDetail(product) {
        let name = product['name'];
        console.log('Request Product Detail FROM WISHLIST: ' + name);

        let page = {
            title: 'ProductDetailsPage',
            product: product,
            wishlist: true
        };

        this.events.publish('open:page', page);
    }

    // TODO - move to controller
    sendList() {
        let page = {
            title: 'SendPage',
            products: this.products
        };

        this.events.publish('open:page', page);
    }

    ionViewDidEnter() {
        this.onscreen = true;
    }

    ionViewWillLeave() {
        this.onscreen = false;
    }

    loadMoreWishlistProducts(infiniteScroll) {
        this.infiniteScroll = infiniteScroll;

        this.events.publish('get:moreWishlistProducts');
    }

    stopScrolling() {
        if (this.infiniteScroll) {
            this.infiniteScroll.complete();
        }
    }

}
