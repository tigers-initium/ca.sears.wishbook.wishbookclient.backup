import {Component} from '@angular/core';
import {ProductCard} from '../../components/ProductCard/ProductCard.ts';
import {Events, NavParams} from 'ionic-angular';
import {TranslatePipe} from 'ng2-translate';

@Component({
    templateUrl: 'build/pages/list/list.html',
    directives: [ProductCard],
    pipes: [TranslatePipe]
})
export class ListPage {
    selectedProduct: any;
    products: Array<{title: any, name: any, url: any, inWishlist: boolean}>;
    onscreen: boolean;
    infiniteScroll: any;

    constructor(public events: Events,
                public navParams: NavParams) {
        console.log('LIST PAGE');

        this.products = [];
        this.onscreen = false;

        this.showAllProducts(navParams.get('page'));

        this.subscribeToEvents();
    }

    subscribeToEvents() {
      this.events.subscribe('load:moreProducts', (page) => {
        this.showAllProducts(page[0]);
      });
    }

    showAllProducts(page) {
        let products = page.products;

        if (page.firstLoad) {
            this.products = [];
        }

        for (let i = 0; i < products.length; i++) {
            let product = products[i];
            this.products.push({
                title: product._id,
                name: product.name,
                url: product.url,
                inWishlist: product.inWishlist
            });
        }

        this.stopScrolling();
    }

    showProductDetail(product) {
        let name = product['name'];
        console.log('Show Product Detail: ' + name);

        let page = {
            title: 'ProductDetailsPage',
            product: product,
            wishlist: false
        };

        this.events.publish('open:page', page);
    }

    showWishList() {
        this.events.publish('request:wishlistPage');
    }

    ionViewDidEnter() {
        this.onscreen = true;
    }

    ionViewWillLeave() {
        this.onscreen = false;
    }

    loadMoreProducts(infiniteScroll) {
        this.infiniteScroll = infiniteScroll;

        this.events.publish('get:moreProducts');
    }

    stopScrolling() {
        if (this.infiniteScroll) {
            this.infiniteScroll.complete();
        }
    }

}
