import {Component} from '@angular/core';
import {TranslatePipe} from 'ng2-translate';
import {Events, App} from 'ionic-angular';
import {SettingsEmailForm} from '../../components/SettingsEmailForm/SettingsEmailForm';

@Component({
    templateUrl: 'build/pages/settings/settings.html',
    pipes: [TranslatePipe],
    directives: [SettingsEmailForm]
})
export class SettingsPage {

}
