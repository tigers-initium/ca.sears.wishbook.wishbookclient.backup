import {Component} from '@angular/core';
import {TranslatePipe} from 'ng2-translate';
import {ProfileHeaderComponent} from '../../components/ProfileHeader/ProfileHeader';

@Component({
    templateUrl: 'build/pages/profile-settings/profile-settings.html',
    inputs: ['profile'],
    pipes: [TranslatePipe],
    directives: [ProfileHeaderComponent]
})
export class ProfileSettingsPage {

    public profile: Object;
    constructor() {
        this.profile = {
            doc: {
                age: '12',
                name: 'Brendan Testing'
            },
            id: 'abc123',
            key: 'abc123secret',
            photo: 'img/icons/png/generic/default/generic@2x.png'
        }
    }
}
