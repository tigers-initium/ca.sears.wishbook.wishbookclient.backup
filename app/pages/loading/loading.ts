import {Component} from '@angular/core';
import {TranslatePipe} from 'ng2-translate';
import {Storage, LocalStorage, Events, App} from 'ionic-angular';
import {Sandbox} from '../../sandboxes/Sandbox';
import {LoadingIndicator} from '../../components/LoadingIndicator/LoadingIndicator';

@Component({
    templateUrl: 'build/pages/loading/loading.html',
    pipes: [TranslatePipe],
    directives: [LoadingIndicator]
})
export class LoadingPage {

    local: Storage;

    constructor(public sanbox: Sandbox,
                public events: Events,
                public app: App) {
        this.local = new Storage(LocalStorage);
        this.local.get('loggedIn').then((isLoggedin) => {
            let page = {
                title: ''
            };
            if (isLoggedin) {
                page.title = 'Profiles';
                this.events.publish('open:page', page);
            } else {
                page.title = 'Onboarding';
                this.events.publish('open:page', page);
            }
        });

    }
}
