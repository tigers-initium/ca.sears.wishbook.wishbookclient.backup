import {Component} from '@angular/core';
// import {IONIC_DIRECTIVES, NavParams} from 'ionic-angular';
import {NavParams, Events} from 'ionic-angular';
import {TranslatePipe} from 'ng2-translate';

@Component({
    templateUrl: 'build/pages/send/send.html',
    pipes: [TranslatePipe]
})
export class SendPage {

    constructor(public navParams: NavParams,
                public events: Events) {
    }

    // TODO - move to controller
    restartApp() {
        let page = {
            title: 'root'
        };

        this.events.publish('open:page', page);
    }

}
