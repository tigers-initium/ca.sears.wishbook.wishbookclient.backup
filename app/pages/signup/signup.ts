import {Component, ViewChild} from '@angular/core';
import {SignupFormComponent} from '../../components/SignupForm/SignupForm';
import {SignupNameComponent} from '../../components/Signup/SignupName/SignupName';
import {SignupAgeComponent} from '../../components/Signup/SignupAge/SignupAge';
import {SignupGenderComponent} from '../../components/Signup/SignupGender/SignupGender';
import {SignupInterestsComponent} from '../../components/Signup/SignupInterests/SignupInterests';
import {IONIC_DIRECTIVES, NavParams, Events, Slides} from 'ionic-angular';
import {sanitizeString} from '../../components/Validators/Validators';

@Component({
    templateUrl: 'build/pages/signup/signup.html',
    directives: [IONIC_DIRECTIVES, SignupFormComponent, SignupNameComponent, SignupAgeComponent, SignupGenderComponent, SignupInterestsComponent]
})
export class SignupPage {

    @ViewChild('SignupSlider') slider: Slides;
    public options: any;
    public swiper: any;

    public name: string;
    public age: number;
    public gender: string;
    public interests: Array<string>;
    public skippable: boolean;
    public sent: boolean;

    constructor(public navParams: NavParams,
                public events: Events) {
        this.skippable = false;
        this.sent = false;
        this.subscribeToEvents();

        let parent = this;
        this.options = {
            onInit: function (slides) {
                parent.lockSlider(slides);
            }
        };
        console.log(this.options);
    }

    // ngOnInit() {
    //     let slide = this.slider;
    //     setTimeout(function () {
    //         let swiper = slide.getSlider();
    //         swiper.lockSwipeToNext();
    //     }, 1000);
    // }

    subscribeToEvents() {
        this.events.subscribe('signup:next', data => this.slideNext());
        this.events.subscribe('signup:save::name', data => this.saveName(data[0]));
        this.events.subscribe('signup:save::age', data => this.saveAge(data[0]));
        this.events.subscribe('signup:save::gender', data => this.saveGender(data[0]));
    }

    slideNext() {
        this.unlockSlider();
        this.slider.slideNext();
    }

    lockSlider(slides) {
        this.swiper = slides;
        this.swiper.lockSwipeToNext();
    }

    unlockSlider() {
        this.swiper.unlockSwipeToNext();
        this.skippable = true;
    }

    saveName(data) {
        let name = data['name'].toString();
        name = sanitizeString(name);
        this.name = name;
    }

    saveAge(data) {
        let age = data['age'];
        this.age = age;
    }

    saveGender(data) {
        let gender = data;
        if(gender !== null) {
            gender = gender.toString();
        }
        this.gender = gender;
        this.createProfile();
    }

    createProfile() {
        let profile = {
            "name": this.name,
            "age": this.age,
            "gender": this.gender
        };
        let results = JSON.stringify(profile);

        if (this.sent !== true) {
            console.log("ASL?", this.name + ": " + this.age + " / " + this.gender + " / Toronto");
            this.events.publish('request:createProfile', results);
            this.sent = true;
        }
    }

}
