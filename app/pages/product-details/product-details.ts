import {Component} from '@angular/core';
import {IONIC_DIRECTIVES, NavParams, Events} from 'ionic-angular';
import {TranslatePipe} from 'ng2-translate';
import {ProductCarousel} from '../../components/ProductCarousel/ProductCarousel';
import {ProductDetailsSaveToggle} from '../../components/ProductDetailsSaveToggle/ProductDetailsSaveToggle';

@Component({
    templateUrl: 'build/pages/product-details/product-details.html',
    directives: [IONIC_DIRECTIVES, ProductCarousel, ProductDetailsSaveToggle],
    pipes: [TranslatePipe]
})
export class ProductDetailsPage {

    product: any;
    isSaved: boolean;
    origin: string;
    wishListToggle: boolean;
    exiting: boolean;

    constructor(public navParams: NavParams,
                public events: Events) {
        console.log('PRODUCT DETAILS');
        // If we navigated to this page, we will have an item available as a nav param
        this.product = navParams.get('product');

        this.isSaved = this.product.inWishlist || false;
        this.wishListToggle = navParams.get('wishlist');
        this.exiting = false;
    }

    toggleSelected() {
        // Optimistic UI - set saved status on click
        this.isSaved = !this.isSaved;
        let id = this.product['title'];
        // Send save event
        if (this.isSaved) {
            this.events.publish('toggle:saveWishlistItem', id);
        } else {
            this.events.publish('toggle:removeWishlistItem', id);
        }
    }

    closeDetails() {
        this.events.publish('close:page');
    }

    ionViewDidEnter() {
        console.log("Product Details Page - View Entered");
    }

    ionViewWillLeave() {
        console.log("Goodbye!");
        console.log(this);
        this.exiting = true;
    }

}
