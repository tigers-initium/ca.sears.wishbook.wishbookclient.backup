# SEARS WISH BOOK

This is the app version of the Sears Wish Book catalogue.

## Rules for Working with Git
1. Create a branch with your name or bitbucket username
2. Only develop in your branch
3. `One` commit = `One` bug fix only (avoid many changes in one commit)
4. Commit message should inform reader of what was fixed. Add JIRA bug number where possible
5. Make sure your branch is up-to-date with the latest `dev` branch
6. Test you code on your branch
7. Notify Delivery lead of your Change
    * Your branch will then merged into dev
    * Further tested
    * Finally the whole project will be released with a new version number. Using SemVer
Which will then be merged into master

The following technologies are involved:

#### Languages
ES6 + TypeScript

#### Design Pattern
* Large scale JavaScript: https://addyosmani.com/largescalejavascript/
* https://github.com/aranm/scalable-javascript-architecture
* https://addyosmani.com/resources/essentialjsdesignpatterns/book/

#### Frameworks
* Node.js
* Ionic 2
* Angular 2

#### Testing
* Protractor (Dev) - http://www.protractortest.org/#/
* User Test/ Usability Tests (Post dev)
* VWO (?)

#### Documentation
* Documentation.js - http://documentation.js.org/

#### Templates
* Ionic

#### Style guides
* Microsoft - Typescript - https://github.com/Microsoft/TypeScript/wiki/Coding-guidelines

#### Process Management
* Ionic - http://ionicframework.com/docs/v2/cli/
